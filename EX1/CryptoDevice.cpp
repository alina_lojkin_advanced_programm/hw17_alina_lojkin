#include "CryptoDevice.h"

//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.

std::string CryptoDevice::encryptAES(std::string plainText)
{

    std::string cipherText;
	int num = 0;
	std::cin >> num;
	int i = 0;
	for (i = 0; (i < 100 && plainText[i] != '\0'); i++)
		cipherText[i] = plainText[i] + num;

	return cipherText;
}

std::string CryptoDevice::decryptAES(std::string cipherText)
{
	
    std::string decryptedText;
	int i = 0;
	for (i = 0; (i < 100 && cipherText[i] != '\0'); i++)
		decryptedText[i] = cipherText[i] - 2;
	return decryptedText;

}